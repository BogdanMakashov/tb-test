import './firebase.js';
import Vue from 'vue';
import App from './App.vue';
import VueFire from "vuefire";
import QuotesList from './components/QuotesList.vue';
import Quote from './components/Quote.vue';
import AddForm from './components/AddForm.vue';

Vue.use(VueFire);

Vue.component('QuotesList', QuotesList);
Vue.component('Quote', Quote);
Vue.component('AddForm', AddForm);


new Vue({
  el: '#app',
  render: h => h(App)
})
