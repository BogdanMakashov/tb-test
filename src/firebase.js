import { initializeApp } from "firebase";

const app = initializeApp({
    apiKey: "AIzaSyCmLTFNlRIVaCxsGNrFIuKLxOzHCC_5o30",
    authDomain: "vue-test-3bb2a.firebaseapp.com",
    databaseURL: "https://vue-test-3bb2a.firebaseio.com",
    projectId: "vue-test-3bb2a",
    storageBucket: "vue-test-3bb2a.appspot.com",
    messagingSenderId: "382474966418"
});

export const db = app.database();
export const quotesRef = db.ref('quotes');